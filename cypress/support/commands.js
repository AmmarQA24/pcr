import incidentDetails from '../fixtures/incidentDetails.json'
import outcomeDetails from '../fixtures/outcomeDetails.json'
import assessmentsDetails from '../fixtures/assessmentDetails.json'
import patientDetails from '../fixtures/patientDetails.json'

Cypress.Commands.add('login', (emailSelector, passwordSelector, loginButtonSelector, username, password) => {
    cy.get(emailSelector).type(username)
    cy.get(passwordSelector).type(password)
    cy.get(loginButtonSelector).click()
  })

  Cypress.Commands.add('fillInIncident', () => {
    cy.get(incidentDetails.dispositionDropdownSelector).select(incidentDetails.dispositionOptionSelector)
    cy.get(incidentDetails.responseUrgencyDropdownSelector).select(incidentDetails.responseUrgencyOptionSelector)
    cy.get(incidentDetails.cmsLevelDropdownSelector).select(incidentDetails.cmsLevelOptionSelector)
    cy.get(incidentDetails.natureOfIncidentDropdownSelector).select(incidentDetails.natureOfIncidentOptionSelector)
    cy.get(incidentDetails.sceneAddressLinkSelector).click()
    cy.get(incidentDetails.sceneAddressStreetInputSelector).type(incidentDetails.sceneAddressStreet)
    cy.get(incidentDetails.sceneAddressCityInputSelector).type(incidentDetails.sceneAddressCity)
    cy.get(incidentDetails.sceneAddressStateDropdownSelector).select(incidentDetails.sceneAddressCaliforniaOptionSelector, {force: true})
    cy.get(incidentDetails.sceneAddressZipCodeInputSelector).type(incidentDetails.sceneAddressZipCode)
    cy.get("button").contains("Close").click({force: true})
    cy.get(incidentDetails.locationTypeDropdownSelector).select(incidentDetails.locationTypeGymOptionSelector, {force: true})
    cy.get(incidentDetails.vehicleNumberDropdownSelector).select(incidentDetails.vehicleNumberOptionSelector, {force: true})
    cy.get(incidentDetails.levelOfCareDropdownSelector).select(incidentDetails.levelOfCareOptionSelector)
    cy.get(incidentDetails.modeToSceneDropdownSelector).select(incidentDetails.modeToSceneOptionSelector)
    cy.get(incidentDetails.modeDescriptorsDropdownSelector).select(incidentDetails.modeDescriptorsOptionSelector, {force: true})
    cy.get(incidentDetails.serviceRequestedDropdownSelector).select(incidentDetails.serviceRequestedOptionSelector, {force: true})
    cy.get(incidentDetails.newCrewButtonSelector).click()
    cy.get(incidentDetails.crewDropdownSelector).select(incidentDetails.crewOptionSelector, {force: true})
    cy.get(incidentDetails.roleDropdownSelector).select(incidentDetails.roleOptionSelector, {force: true})
    cy.get("#crew div button").contains("Save").click({force: true});
    cy.get(incidentDetails.unitNotifiedInputSelector).type("11:11:11")
    cy.get(incidentDetails.enRouteInputSelector).type("11:11:11")
    cy.get(incidentDetails.onSceneInputSelector).type("11:11:11")
    cy.get(incidentDetails.destinationInputSelector).type("11:11:11")
    cy.get(incidentDetails.inServiceInputSelector).type("11:11:11")
  })

  Cypress.Commands.add('saveChanges', () => {
    cy.get(incidentDetails.formOptionsSelector).click()
    cy.get(incidentDetails.saveFormSelector).click()
    cy.get("i.fa-cloud-upload").should('contain.attr', 'style', 'font-size: x-large; cursor: pointer; color: green;')
  })

  Cypress.Commands.add('checkInToCloud', () => {
    cy.get(incidentDetails.formOptionsSelector).click()
    cy.get(incidentDetails.backButtonSelector).click()
    cy.get("button").contains("Check in to Cloud").click();
  })

  Cypress.Commands.add('navigateToNext', () => {
    cy.get(incidentDetails.errorsListSelector).click();
    cy.get(incidentDetails.errorSelector).eq(0).click();
    cy.get("i.fa-close").click();
  })

  Cypress.Commands.add('fillInPatient', () => {
    cy.get(patientDetails.firstNameInputSelector).type(patientDetails.firstName)
    cy.get(patientDetails.lastNameInputSelector).type(patientDetails.lastName)
    cy.get(patientDetails.raceDropdownSelector).select(patientDetails.race, {force: true})
    cy.get(patientDetails.genderDropdownSelector).select(patientDetails.gender, {force: true})
  })

  Cypress.Commands.add('fillInAssessments', () => {
    cy.get(assessmentsDetails.assessmentsLink).click()
    cy.get(assessmentsDetails.primaryImpressionsDropdownSelector).select(assessmentsDetails.primarySymptom, {force: true})
    cy.get(assessmentsDetails.primaryImpressionsDropdownSelector).select(assessmentsDetails.primaryImpressions, {force: true})
    cy.get(assessmentsDetails.secondaryImpressionsDropdownSelector).select(assessmentsDetails.secondaryImpressions, {force: true})
    cy.get(assessmentsDetails.possibleInjuryDropdownDropdownSelector).select(assessmentsDetails.possibleInjury, {force: true})
  })

  Cypress.Commands.add('fillInOutcome', () => {
    cy.get(outcomeDetails.outcomeLink).click()
    cy.get(outcomeDetails.destinationAddressLinkSelector).click()
    cy.get(outcomeDetails.destinationAddressStreetInputSelector).type(outcomeDetails.destinationAddressStreet)
    cy.get(outcomeDetails.destinationAddressCityInputSelector).type(outcomeDetails.destinationAddressCity)
    cy.get(outcomeDetails.destinationAddressStateDropdownSelector).select(outcomeDetails.destinationAddressCaliforniaOptionSelector, {force: true})
    cy.get(outcomeDetails.destinationAddressZipCodeInputSelector).type(outcomeDetails.destinationAddressZipCode)
    cy.get("button").contains("Close").click({force: true})
  })

  Cypress.Commands.add('updateSceneAddress', () => {
    cy.get(incidentDetails.sceneAddressLinkSelector).click()
    cy.get(incidentDetails.sceneAddressStreetInputSelector).clear()
    cy.get(incidentDetails.sceneAddressStreetInputSelector).type(incidentDetails.sceneAddressUpdateStreet)
    cy.get(incidentDetails.sceneAddressCityInputSelector).clear()
    cy.get(incidentDetails.sceneAddressCityInputSelector).type(incidentDetails.sceneAddressUpdateCity)
    cy.get(incidentDetails.sceneAddressStateDropdownSelector).select(incidentDetails.sceneAddressCaliforniaOptionSelector, {force: true})
    cy.get(incidentDetails.sceneAddressZipCodeInputSelector).clear()
    cy.get(incidentDetails.sceneAddressZipCodeInputSelector).type(incidentDetails.sceneAddressUpdateZipCode)
    cy.get("button").contains("Close").click({force: true})
  })
