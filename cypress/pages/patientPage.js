class PatientPage {
  
    get firstNameInput() {
      return cy.get("#control-ePatient_03");
    }
  
    get lastNameInput() {
      return cy.get("#control-ePatient_02");
    }

    get raceDropdown() {
      return cy.get("#control-ePatient_14");
    }

    get genderDropdown() {
      return cy.get("#control-ePatient_13");
    }

    fillPatientDetails(firstName, lastName, race, gender) {
      this.firstNameInput.type(firstName);
      this.lastNameInput.type(lastName);
      this.raceDropdown.select(race, {force: true});
      this.genderDropdown.select(gender, {force: true});
  
      return this;
    }
    
  }

  export default PatientPage;