class AssessmentPage {

    get primarySymptomDropdown() {
      return cy.get("#control-eSituation_09");
    }
  
    get secondaryImpressionsDropdown() {
      return cy.get("#control-eSituation_12");
    }

    get primaryImpressionsDropdown() {
      return cy.get("#control-eSituation_11");
    }

    get possibleInjuryDropdown() {
      return cy.get("#control-eSituation_02");
    }

    fillAssessmentDetails(primarySymptom, primaryImpressions, secondaryImpressions, possibleInjury) {
      this.primarySymptomDropdown.select(primarySymptom, {force: true});
      this.primaryImpressionsDropdown.select(primaryImpressions, {force: true});
      this.secondaryImpressionsDropdown.select(secondaryImpressions, {force: true});
      this.possibleInjuryDropdown.select(possibleInjury, {force: true});
  
      return this;
    }
    
  }

  export default AssessmentPage;