import login from '../../fixtures/login.json'

describe('Add new PCR', () => {

    it('Should allow user to fill in accident details', () => {
        cy.visit("https://cloudpcrv3.azurewebsites.net/Mpa/MyPcrs");
        cy.login(login.emailSelector, login.passwordSelector, login.loginButtonSelector, login.email, login.password);
        cy.get('body').then(($body) => {
            if (!$body.find("a[href='/Mpa/MyPcrs']").length) {
                cy.visit("https://cloudpcrv3.azurewebsites.net/Mpa/MyPcrs");
                cy.login(login.emailSelector, login.passwordSelector, login.loginButtonSelector, login.email, login.password);
            }
          })
        cy.get("a[href='/Mpa/MyPcrs']").click();
        cy.get("#NewPcrButton").click();
        cy.fillInIncident();
        cy.navigateToNext();
    });

    it('Should allow user to fill in patient details', () => {
        cy.fillInPatient();
    });

    it('Should allow user to fill in outcome details', () => {
        cy.fillInOutcome();
        cy.saveChanges();
    });

    it('Should check in changes to cloud', () => {
        cy.checkInToCloud();
        cy.get(login.emailSelector).should('be.visible');
    });

    it('Should allow user to edit scene address', () => {
        cy.visit("https://cloudpcrv3.azurewebsites.net/Mpa/MyPcrs");
        cy.login(login.emailSelector, login.passwordSelector, login.loginButtonSelector, login.email, login.password);
        cy.get("a[href='/Mpa/MyPcrs']").click();
        cy.get("#myPcrs tbody tr:nth-child(8) td:nth-child(3)").click();
        cy.updateSceneAddress();
        cy.saveChanges();
        cy.checkInToCloud();
        cy.get("#NewPcrButton").should('be.visible');
    });

    // no idea why but couldn't make this work, whenever it opens assessments everything stops working- https://www.loom.com/share/ac9e3d36271f40a5b7cc858066ab1b1b
    // it('Should allow user to fill in assessment details', () => {
    //     cy.visit("https://cloudpcrv3.azurewebsites.net/Mpa/MyPcrs");
    //     cy.login(login.emailSelector, login.passwordSelector, login.loginButtonSelector, login.email, login.password);
    //     cy.get("a[href='/Mpa/MyPcrs']").click();
    //     cy.get("#myPcrs tbody tr:nth-child(3) td:nth-child(3)").click();
    //     cy.fillInAssessments();
    //     cy.saveChanges();
    // });

});